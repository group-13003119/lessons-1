import java.util.Scanner;

public class Array {
  public static void main(String[] args) {
    int[][] arr = new int[3][3];

    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < arr[i].length; j++) {
        arr[i][j] = i+j+1;
      }
    }


    for (int i = 0; i < arr.length; i++) {
      System.out.println();
      for (int j = 0; j < arr[i].length; j++) {
        System.out.print(arr[i][j]);
      }
    }

    System.out.println();

    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < arr[i].length; j++) {
        if (i == j){
          System.out.println(arr[i][j]);
        }
      }

    }

  }

  public static void method_1() {
    char[] array = new char[10];
    String words = "abcd bcde cdef";

    char[] charArray = words.toCharArray();
    PrintUtil.printAllChar(charArray);
  }
}
