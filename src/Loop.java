import java.util.Scanner;

public class Loop {

  public static void main(String[] args) {
    int result = sum(4);
    System.out.println(result);
  }

  public static int sum(int k) {
    if (k > 0) {
      return k + sum(k - 1);
    } else {
      return 0;
    }
  }

  public static int foo(int i) {
    System.out.println("Index: " + i);

    if (i >= 4) {
      return i;
    }

    return foo(i + 1);
  }

  public static double topla(double toplanan_1, double toplanan_2) {
    double cem = toplanan_1 + toplanan_2;
    return cem;
  }

  public static double chixma(double a, double b) {
    double ferq = a - b;
    return ferq;
  }

  public static double vurma(double c, double d) {
    double result = c * d;
    return result;
  }

  public static double bolme(double b, double a) {
    double result = b / a;
    return result;
  }

}
